import java.util.*;

public class Programm {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Guten Tag! Wie lautet ihr Name?");
		String name = sc.next();
		System.out.println("Gut und wie Alt sind sie?");
		int alter = sc.nextInt();
		
		System.out.println("Vielen Dank! Sie sind " + name + " und ihr Alter betr�gt "+ alter + "\n");
		
		sc.close();
		
	}

}
