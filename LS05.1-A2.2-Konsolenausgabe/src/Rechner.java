import java.util.Scanner;

public class Rechner{

 public static void main(String[] args){

 Scanner myScanner = new Scanner(System.in);

 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

 int zahl1 = myScanner.nextInt();

 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

 int zahl2 = myScanner.nextInt();

 // Die Addition der Variablen zahl1 und zahl2
 // wird der Variable ergebnis zugewiesen.
 int ergebnisSumme = zahl1 + zahl2;
 int ergebnisSubtrahend = zahl1 - zahl2;
 int ergebnisProdukt = zahl1 * zahl2;
 int ergebnisDivsor = zahl1 / zahl2;

 System.out.println("Ergebnis der Addition lautet: " + zahl1 + " + " + zahl2 + " = " + ergebnisSumme);
 System.out.println("Ergebnis der Subtraktion lautet: " + zahl1 + " - " + zahl2 + " = " + ergebnisSubtrahend);
 System.out.println("Ergebnis der Multiplikation lautet: " + zahl1 + " x " + zahl2 + " = " + ergebnisProdukt);
 System.out.println("Ergebnis der Division lautet: " + zahl1 + " / " + zahl2 + " = " + ergebnisDivsor);

 myScanner.close();

 }
}