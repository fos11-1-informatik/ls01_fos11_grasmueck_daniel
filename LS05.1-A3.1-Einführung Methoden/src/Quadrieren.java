public class Quadrieren {
   
  public static void main(String[] args) {

    // (E) "Eingabe"
    // Wert f�r x festlegen:
    // ===========================
    System.out.println("Dieses Programm berechnet die Quadratzahl x^2");
    System.out.println("---------------------------------------------");
    double x = 5.0;
    double ergebnis;
        
    // (V) Verarbeitung
    // Mittelwert von x und y berechnen:
    // ================================
    ergebnis = quadrieren(x);

    // (A) Ausgabe
    // Ergebnis auf der Konsole ausgeben:
    // =================================
    System.out.printf("x^2 = %.2f ^2 = %.2f\n", x, ergebnis);
  } 
  public static double quadrieren(double x){

    double ergebnis = x * x;

    return ergebnis;

  }

}