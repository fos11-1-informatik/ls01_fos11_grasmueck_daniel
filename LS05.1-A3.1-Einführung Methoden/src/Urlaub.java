import java.util.*;

public class Urlaub {

    public static void main (String[] args) {
        
        Scanner sc = new Scanner(System.in);

        double usd = 1.22;
        double jpy = 126.50;
        double gbp = 0.89;
        double chf = 1.08;
        double sek = 10.10;
        String landkuerzel;
        
        double betrag = 0;
        double reisebudget;
        
        System.out.println("Bitte zahlen sie ihren Betrag in Euro ein: "
                + "\n Bitte beachten sie das sie genug Budget mitnehmen "
                + "\n Rechnen sie bitte immer 100� Reserve ein");
            betrag = sc.nextDouble();
            reisebudget = betrag - 100;
            double reserveEuro = 100;
            System.out.println("Ihr Reisebudget betr�gt: " + reisebudget + " EUR \n"
                    + "Ihre Reserve betr�gt: " + reserveEuro + " EUR \n");
            
        if (reserveEuro >= 100 && reisebudget >= 100) {
            System.out.println("Bitte geben sie das Land ein in welches sie umtauschen wollen");
            System.out.print("Unsere derzeitigen Umtauschkurse: "
                    + "\n    1 EUR > " + usd + " USD "
                    + "\n    1 EUR > " + jpy + " jpy "
                    + "\n    1 EUR > " + gbp + " GBP " 
                    + "\n    1 EUR > " + chf + " CHF "
                    + "\n    1 EUR > " + sek + " SEK "
                    + "\nBeachten sie bitte die Schreibweise usd oder USD"
                    + "\nGeben sie bitte nun ihr gew�nschtes Land ein: ");
            landkuerzel = sc.next();
            
            if(landkuerzel.equals("usd")||landkuerzel.equals("USD")) {
                double umrechnenUSD = berechneUSD(reisebudget, usd);
                System.out.println("Sie bekommen " + umrechnenUSD + " USD ");
            }
            if(landkuerzel.equals("jpy")||landkuerzel.equals("JPY")) {
                double umrechnenJPY = berechneJPY(reisebudget, jpy);
                System.out.println("Sie bekommen " + umrechnenJPY + " JPY ");
            }
            if(landkuerzel.equals("gbp")||landkuerzel.equals("GBP")) {
                double umrechnenGBP = berechneGBP(reisebudget, gbp);
                System.out.println("Sie bekommen " + umrechnenGBP + " GBP ");
            }
            if(landkuerzel.equals("chf")||landkuerzel.equals("CHF")) {
                double umrechnenCHF = berechneCHF(reisebudget, chf);
                System.out.println("Sie bekommen " + umrechnenCHF + " CHF ");
            }
            if(landkuerzel.equals("sek")||landkuerzel.equals("SEK")) {
                double umrechnenSEK = berechneSEK(reisebudget, sek);
                System.out.println("Sie bekommen " + umrechnenSEK + " SEK ");
            }
        } else {
            System.out.println("Sie f�hren nicht genug Geld mit. "                    
                    + "\nBitte weisen sie sich am Flugschalter ein");
                    System.exit(0);
        }sc.close();
    }
    public static double berechneUSD(double reisebudget, double usd) {
        double umrechnenUSD = reisebudget * usd;
        return umrechnenUSD;
    }
    public static double berechneJPY(double reisebudget, double jpy) {
        double umrechnenJPY = reisebudget * jpy;
        return umrechnenJPY;
    }
    public static double berechneGBP(double reisebudget, double gbp) {
        double umrechnenGBP = reisebudget * gbp;
        return umrechnenGBP;
    }
    public static double berechneCHF(double reisebudget, double chf) {
        double umrechnenCHF = reisebudget * chf;
        return umrechnenCHF;
    }
    public static double berechneSEK(double reisebudget, double sek) {
        double umrechnenSEK = reisebudget * sek;
        return umrechnenSEK;
    }
    
}